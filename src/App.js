import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'reactstrap';
import Body from './Components/body';
function App() {
  return (
    <Container style={{backgroundColor:"#bdc3c7",marginTop:"100px", padding: "20px"}}>
        <Body/>
    </Container>
  );
}

export default App;
