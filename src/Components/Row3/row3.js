import {Row, Label, Input, Col, Form, FormGroup} from "reactstrap"

const BodyRow3 = () => {
    return (
        <>
        <Row className="mt-3">
            <Form>
                <FormGroup row>
                    <Col sm={2}>
                        <Label for="job">
                            Công việc
                        </Label>
                    </Col>
                    <Col sm={10}>
                        <Input id="job" name="text" type="textarea" style={{height: "150px"}}/>
                    </Col>
                </FormGroup>
            </Form>
        </Row>
        </>
    )
}
export default BodyRow3;