import { Row, Col } from "reactstrap"

const BodyRow1 = () => {
    return (
        <>
            <Row className="text-center">
                <Col md={{
                    offset: 3,
                    size: 6
                }}
                    sm="12">
                    <h4>HỒ SƠ NHÂN VIÊN</h4>
                </Col>
            </Row>
        </>
    )
}
export default BodyRow1;