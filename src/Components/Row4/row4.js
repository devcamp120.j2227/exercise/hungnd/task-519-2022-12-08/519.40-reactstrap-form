import {Row, Button} from "reactstrap"

const BodyRow4 = () => {
    return (
        <>
            <Row sm={12} >
                <div className="d-flex justify-content-end">
                    <Button style={{margin:"2px"}} color="success">
                            Chi Tiết
                    </Button>
                    <Button style={{margin:"2px"}} color="success">
                            Kiểm Tra
                    </Button>
                </div>
            </Row>
        </>
    )
}
export default BodyRow4;